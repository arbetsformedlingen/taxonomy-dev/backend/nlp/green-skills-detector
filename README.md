# green-skill-detector

## Usage

Process texts:
```
make analyze-files
```

Process educations:
```
make analyze-myh-spreadsheet
make render-myh-excel
```
The output document is located at `work/myh_analysis.xlsx`.

## Links

* [k-NEAREST NEIGHBOUR KERNEL DENSITY ESTIMATION, THE CHOICE OF OPTIMAL k](https://sciendo.com/pdf/10.2478/v10127-011-0035-z)
* [Multivariate k-nearest neighbor density estimates](https://www.sciencedirect.com/science/article/pii/0047259X79900654/pdf?md5=f9f73fd14783d19d94f5675baf03780f&pid=1-s2.0-0047259X79900654-main.pdf&_valck=1)
* [Python KDE](https://scikit-learn.org/stable/modules/density.html)
