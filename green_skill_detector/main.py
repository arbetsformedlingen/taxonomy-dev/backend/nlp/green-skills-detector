from green_skill_detector import core
import argparse
import os
import pathlib

def add_action(action_name, help_message):
    parser.add_argument(action_name, help=help_message,
                        action="store_true")

parser = argparse.ArgumentParser(description='Detect green skills')
add_action("--encode-skills", "Encode all the skills")
add_action("--analyze-files", "Process all files")
add_action("--analyze-myh-spreadsheet", "Process the MYH spreadsheet")
add_action("--render-excel", "Render excel")
parser.add_argument("--output-path", help="General location for the output")
parser.add_argument("--input-path", help="General location for the input")
args = parser.parse_args()

def main():
    print("Current working directory: " + os.getcwd())
    settings = core.make_default_settings()
    context = core.Context(settings)
    if args.encode_skills:
        context.load_sbert_model()
        context.load_skills()
        context.encode_skills()
    if args.analyze_files:
        context.load_sbert_model()
        context.load_skills()
        context.load_encoded_skills()
        detector = context.make_detector()
        proc = core.DetectorFileProcessor(detector)
        proc.process_files(args.input_path, args.output_path)
    if args.analyze_myh_spreadsheet:
        context.load_sbert_model()
        context.load_skills()
        context.load_encoded_skills()
        detector = context.make_detector()
        processor = core.MyhProcessor(detector)
        educations = core.load_myh_educations(pathlib.Path(args.input_path))
        processor.process_educations(educations, args.output_path)
    if args.render_excel:
        core.render_results_to_excel(args.output_path, args.input_path)

if __name__ == "__main__":
    main()
