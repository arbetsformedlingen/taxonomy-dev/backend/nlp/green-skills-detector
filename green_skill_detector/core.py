dev = False
## Code
import pathlib
import openpyxl
from sentence_transformers import SentenceTransformer
import numpy as np
import os
from sklearn.neighbors import KernelDensity, NearestNeighbors
import json


def write_json(filename, data):
    with open(filename, "w") as f:
        json.dump(data, f)

def read_json(filename):
    assert(isinstance(filename, str))
    with open(filename, "r") as f:
        return json.load(f)
    
def slurp(filename):
    with open(filename, "r") as f:
        return f.read()

def spit(filename, s):
    with open(filename, "w") as f:
        f.write(s)
    
punctuations = set(".!?,;:")

def make_default_settings():
    return {"skills_path": pathlib.Path("work", "skills.xlsx"),
            "sbert_model_name": "KBLab/sentence-bert-swedish-cased",
            "sbert_matrix_path": pathlib.Path("work", "skills_sbert_matrix.npy"),
            "bandwidth": 0.4,
            "kernel": "gaussian",
            "detector": "full-text",
            "n_neighbors": 30,
            "max_window": 30}
    
def get_character_class(c):
    if c in punctuations:
        return "punctuation"
    elif c.isspace():
        return "space"
    else:
        return "other"

def get_text_bounds(text):
    n = len(text)
    inner_bounds = [i+1 for i in range(0, n-1) if get_character_class(text[i]) != get_character_class(text[i+1])]
    return [i for group in [[0], inner_bounds, [n]] for i in group]

def generate_all_slices(text, settings):
    max_window = settings["max_window"]
    bounds = get_text_bounds(text)
    n = len(bounds)
    # For every starting point
    slices = []
    for i in range(n):
        # For every end point
        for j in range(i+1, min(i + max_window + 1, n)):
            has_other = False
            for k in range(i, j):
                if get_character_class(text[bounds[k]]) == "other":
                    has_other = True
                    break


            if has_other:
                lower = bounds[i]
                upper = bounds[j]
                slices.append({"interval": (lower, upper), "substring": text[lower:upper]})

    return slices
                
def generate_one_slice(text):
    return [{"interval": (0, len(text)), "substring": text}]
    


def sheet_to_maps(sheet):
    all_rows = list(sheet.rows)
    header_row = all_rows[0]
    rows = all_rows[1:]

    def map_from_row(data_row):
        return {header_cell.value:data_cell.value for (header_cell, data_cell) in zip(header_row, data_row)}
    
    return [map_from_row(data_row) for data_row in rows]

def load_sheet(filepath):
    assert(isinstance(filepath, pathlib.Path))
    wb_obj = openpyxl.load_workbook(filepath) 
    return sheet_to_maps(wb_obj.active)

skill_label_key = 'Grön ESCO-kompetens'

def encode_all_skills(model, skilldata):
    words_to_encode = [x[skill_label_key] for x in skilldata]
    print("Encode {:d} words".format(len(words_to_encode)))
    matrix = model.encode(words_to_encode)
    print("Done")
    return matrix

def normalize_rows(x):
    x_norm = np.linalg.norm(x, axis=1, keepdims=True)
    return x/x_norm

def encode_slices(sbert_model, slices):
    print("Processing text")
    strings = [x["substring"] for x in slices]
    print("Encoding slices")
    matrix = sbert_model.encode(strings)
    print("Done encoding")
    return {"slices": slices, "matrix": matrix}


class SkillLookup:
    def __init__(self, sbert_model, skilldata, encoded_skills, settings):
        self.sbert_model = sbert_model
        self.skilldata = skilldata
        self.encoded_skills = normalize_rows(encoded_skills)
        self.nn = NearestNeighbors(n_neighbors=settings["n_neighbors"]).fit(self.encoded_skills)

    def evaluate_slices(self, encoded_data):
        slices = encoded_data["slices"]
        n = len(slices)
        dists, inds = self.nn.kneighbors(encoded_data["matrix"])
        result = [{**slices[i],
                   "dists": dists[i, :].tolist(),
                   "inds": inds[i, :].tolist(),
                   "skilldata": [{**(self.skilldata[j]), "dist": float(d)}
                                 for (d, j) in zip(dists[i], inds[i, :])]}
                  for i in range(n)]
        for x in result:
            x["cost"] = x["dists"][0]
        return result
        
        
    def find_best_nonoverlapping_slices(self, encoded_data):
        slices = encoded_data["slices"]
        evaled_slices = self.evaluate_slices(encoded_data)
        evaled_slices.sort(key=lambda x: x["cost"])

        textlen = max([x["interval"][1] for x in slices])

        mask = [False]*textlen

        def can_pick_slice(x):
            (lower, upper) = x["interval"]
            for i in range(lower, upper):
                if mask[i]:
                    return False
            for i in range(lower, upper):
                mask[i] = True
            return True

        good_slices = [x for x in evaled_slices if can_pick_slice(x)]
        
        return good_slices

class FullTextSkillDetector:
    def __init__(self, skill_lookup, settings):
        self.skill_lookup = skill_lookup
        self.settings = settings

    def detect_skills(self, text):
        assert(isinstance(text, str))
        input_slices = generate_one_slice(text)
        encoded_sample_text = encode_slices(self.skill_lookup.sbert_model, input_slices)
        return self.skill_lookup.find_best_nonoverlapping_slices(encoded_sample_text)
        

def result_to_string(sliceresult):
    dst = sliceresult["substring"] + "\n";
    dists = sliceresult["dists"]
    skills = sliceresult["skilldata"]
    m = len(dists)
    for i in range(m):
        dst += "  * " + skills[i][skill_label_key] + " -> " + str(dists[i]) + "\n"
    return dst
        
class Context:
    def __init__(self, settings):
        self.settings = settings

    def load_sbert_model(self):
        self.sbert_model = SentenceTransformer(self.settings["sbert_model_name"])

    def load_skills(self):
        self.skilldata = load_sheet(self.settings["skills_path"])

    def encode_skills(self):
        self.encoded_skills = encode_all_skills(self.model, self.skilldata)
        with open(self.settings["sbert_matrix_path"], "wb") as f:
            np.save(f, self.encoded_skills)

    def load_encoded_skills(self):
        with open(self.settings["sbert_matrix_path"], "rb") as f:
            self.encoded_skills = np.load(f)

    def make_skill_lookup(self):
        return SkillLookup(self.sbert_model, self.skilldata, self.encoded_skills, self.settings)

    def make_full_text_skill_detector(self):
        return FullTextSkillDetector(self.make_skill_lookup(), self.settings)

    def make_detector(self):
        det = self.settings["detector"]
        if det == "full-text":
            return self.make_full_text_skill_detector()

        raise RuntimeError("Cannot make detector " + det)
            
        

sample_text = "En solenergiprojektör planlägger och tar fram solenergianläggningar mot kund. Som solenergiprojektör konstruerar du lösningsförslag som uppfyller både kundens och branschens krav vad gäller prestanda och kvalitet. Du gör investeringskalkyler för hela produktlivscykeln och hittar lösningar för att skicka överskottsel till elnätet som ägaren av installationen får ersättning för. Du arbetar inte med monteringen av anläggningen, men administrerar installatörerna som sköter den.    Under utbildningen rustas du med gedigna kunskaper i att självständigt projektera mindre och större solenergianläggningar. Utbildningen passar dig som gillar att vara spindeln i nätet, är bra på kundrelationer och duktig på att ha en helhetssyn över projekt.    Som solenergiprojektör har du chansen att arbeta i en av världens snabbast växande branscher. Intresset för hållbara energilösningar växer hos både privatpersoner och i planeringen av stora byggprojekt. Du blir en nyckelperson i företaget, då den här typen av utbildning har saknats på många håll i landet och ny kompetens är nödvändig för att branschen ska kunna växa i takt med efterfrågan."

def slice_encoded(encoded, l, u):
    return {"slices": encoded["slices"][l:u], "matrix": encoded["matrix"][l:u, :]}

class FileProcessor:
    def __init__(self):
        pass

    def should_process(self, filename):
        return True

    def process_file(self, src_filename, src_path, dst_path):
        pass

    def get_output_filename(self, input_filename):
        return input_filename
    
    def process_files(self, src_directory, dst_directory):
        os.makedirs(dst_directory, exist_ok=True)
        src_files = os.listdir(src_directory)
        for fname in src_files:
            src_path = os.path.join(src_directory, fname)
            out_fname = self.get_output_filename(fname)
            dst_path = os.path.join(dst_directory, out_fname)
            print("Process " + fname)
            print("   From " + src_path)
            print("     to " + dst_path)
            self.process_file(fname, src_path, dst_path)

def clean_result(result):
    return {k:v for (k, v) in result.items() if k != "substring"}

class ResultWrapper:
    def wrap_segments(self, basedata, segments):
        return {**basedata, "segments": [clean_result(x) for x in segments]}

class DetectorFileProcessor(FileProcessor):
    def __init__(self, detector):
        self.detector = detector
        self.wrapper = ResultWrapper()

    def get_output_filename(self, input_filename):
        (base, ext) = os.path.splitext(input_filename)
        return base + ".json"
        
    def process_file(self, src_filename, src_path, dst_path):
        text = slurp(src_path)
        skills = self.detector.detect_skills(text)
        write_json(dst_path, self.wrapper.wrap_segments({"filename": src_filename, "key": src_filename}, skills))

def load_myh_educations(filename):
    return load_sheet(filename)

class MyhProcessor:
    def __init__(self, detector):
        self.wrapper = ResultWrapper()
        self.detector = detector

    def process_educations(self, educations, dst_directory):
        os.makedirs(dst_directory, exist_ok=True)
        counter = 0
        for education in educations:
            print("Process education {:d}/{:d}".format(counter+1, len(educations)))
            code = education["Utbildningsnummer"]
            text = education["Om utbildningen"]
            if isinstance(text, str):
                dst_path = os.path.join(dst_directory, code + ".json")
                results = self.detector.detect_skills(text)
                write_json(dst_path, self.wrapper.wrap_segments({"education-code": code, "key": code, "text": text}, results))
                
            counter += 1

def source_text_table_from_myh_educations(myh_educations):
    return {education["Utbildningsnummer"]:education["Om utbildningen"] for education in myh_educations}

def render_results_to_excel(out_filename, result_directory):
    book = openpyxl.Workbook()
    sheet = book.active
    columns = {"key": 0,
               "text": 1,
               "skill": 2,
               "dist": 3}
    def set_cell(i, j, value):
        assert(isinstance(i, int))
        assert(isinstance(j, int))
        assert(isinstance(value, str))        
        sheet.cell(row=i+1, column=j+1).value = value
    
    row = 0
    for k in columns:
        set_cell(row, columns[k], k)
    row += 1

    filenames = os.listdir(result_directory)
    for filename in filenames:
        data = read_json(os.path.join(result_directory, filename))
        key = data["key"]
        text = data["text"]
        set_cell(row, columns["key"], key)
        set_cell(row, columns["text"], text)
        for segment in data["segments"]:
            for skilldata in segment["skilldata"]:
                set_cell(row, columns["skill"], skilldata[skill_label_key])
                set_cell(row, columns["dist"], str(skilldata["dist"]))
                row += 1
    book.save(out_filename)

## Producing the output
if dev:
    input_directory = "../work/analyzed_educations"
    output_filename = "/tmp/results.xlsx"
    render_results_to_excel(output_filename, input_directory)
    print("Done")

    



## Load the sheet

if dev:
    myh_desc_filename = pathlib.Path("../data/Utbildningsinformation_från_MYH_20210909.xlsx")
    myh_educations = load_myh_educations(myh_desc_filename)
    detector = context.make_full_text_skill_detector()
    processor = MyhProcessor(detector)
    processor.process_educations(myh_educations, "/tmp/myhresults")

        
## Detector

if dev:
    detector = context.make_full_text_skill_detector()
    results = detector.detect_skills(sample_text)

## Processing many things

if dev:
    detector = context.make_full_text_skill_detector()
    processor = DetectorFileProcessor(detector)

    input_directory = "../data/sample_texts"
    output_directory = "/tmp/kattskit"
    processor.process_files(input_directory, output_directory)
    

## Initialization

def fix_path(x):
    if isinstance(x, pathlib.Path):
        return pathlib.Path("..", x)
    else:
        return x

if dev:
    settings = make_default_settings()

    for k in settings:
        settings[k] = fix_path(settings[k])

    context = Context(settings)
    context.load_sbert_model()
    context.load_skills()
    #context.encode_skills()
    context.load_encoded_skills()
    print("Loaded")

## Turn on dev
    
dev = True

    
