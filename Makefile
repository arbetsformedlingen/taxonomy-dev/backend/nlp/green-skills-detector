.PHONY: clean encode-skills analyze-files

clean:
	rm -rf work

work/skills.xlsx:
	mkdir -p work && wget -O work/skills.xlsx https://gitlab.com/arbetsformedlingen/taxonomy-dev/backend/nlp/nlp-education-analyzer/uploads/0828c4e412b093bda03d48447c3b45f0/React_-_gr%C3%B6na_kompetenser_SSYK_-_ESCO.xlsx

work/skills_sbert_matrix.npy: work/skills.xlsx
	python3 green_skill_detector/main.py --encode-skills

encode-skills: work/skills_sbert_matrix.npy

analyze-files: work/skills_sbert_matrix.npy
	python3 green_skill_detector/main.py --analyze-files --input-path data --output-path work/analyzed_files

analyze-myh-spreadsheet: work/skills_sbert_matrix.npy
	python3 green_skill_detector/main.py --analyze-myh-spreadsheet --input-path data/Utbildningsinformation_från_MYH_20210909.xlsx --output-path work/analyzed_educations

render-myh-excel: 
	python3 green_skill_detector/main.py --render-excel --input-path work/analyzed_educations --output-path work/myh_analysis.xlsx
